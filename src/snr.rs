
pub const SNR_CHUNK_SIZE : usize = 76;

use crate::utils::*;
use byteorder::{WriteBytesExt, BigEndian as BE};
use crate::snr_to_wav::WAV_CHUNK_SIZE;

#[allow(non_camel_case_types)]
#[derive(Default, Debug, Copy, Clone, Eq, PartialEq)]
pub struct I_12_4 (i16);

impl I_12_4 {
    pub fn from_i16(v: i16) -> Self { Self{0:v} }
    pub fn from_parts(v12: i16, v4: i8) -> Self { Self{0: (v12 & !0xF) | (v4 & 0xF) as i16} }
    pub fn to_i16(self) -> i16 { self.0 }
    pub fn to_parts(self) -> (i16, i8) { (self.0 & !0xF, (self.0 & 0xF) as i8) }
}

/// in memory representation of snr chunk
#[derive(Default, Debug)]
pub struct SnrChunk {
    pub header: [SnrChunkBlockHeader;4],
    pub block: [[u8;15];4],
}
#[derive(Default, Debug)]
pub struct SnrChunkBlockHeader {
    pub offset: I_12_4,
    pub shift: I_12_4,
}
impl SnrChunk {
    /// write chunk to file
    pub fn write(&self, snr: &mut impl byteorder::WriteBytesExt) -> std::io::Result<usize> {
        for i in 0..4 {
            snr.write_i16::<byteorder::LittleEndian>(self.header[i].offset.to_i16())?;
            snr.write_i16::<byteorder::LittleEndian>(self.header[i].shift.to_i16())?;
        }
        for m in 0..15 {
            for n in 0..4 {
                snr.write_u8(self.block[n][m])?;
            }
        }

        Ok(SNR_CHUNK_SIZE)
    }
    /// write chunk to file
    pub fn to_channel_data(&self, n: usize, channel_data: &mut [i16]) {
        let mut coeff = [[0,0,0,0], [0,0,0,0]];
        let mut shifts = [0,0,0,0];

        let (val, coef) = self.header[n].offset.to_parts();
        for i in 0..2 {
            coeff[i][n] = EA_ADPCM_TABLE[coef as usize + 4 * i];
        }
        let s0 = val;
        channel_data[0] = s0;

        let (val, shift) = self.header[n].shift.to_parts();
        shifts[n] = 20 - shift;
        let s1 = val;
        channel_data[1] = s1;

        for m in 0..15 {
            let s0 = channel_data[2*m+0];
            let s1 = channel_data[2*m+1];
            let byte = self.block[n][m];

            let level = sign_extend(byte >> 4, 4) * (1 << shifts[n] as i32);
            let pred = s1 as i32 * coeff[0][n] + s0 as i32 * coeff[1][n];
            let s2 = av_clip_int16_c((level + pred + 0x80) >>8);
            channel_data[2 + 2 * m + 0] = s2;

            let level = sign_extend(byte, 4) * (1 << shifts[n] as i32);
            let pred = s2 as i32 * coeff[0][n] + s1 as i32 * coeff[1][n];
            let s3 = av_clip_int16_c((level + pred + 0x80) >>8);
            channel_data[2 + 2 * m + 1] = s3;
        }
    }
}

///
pub(crate) fn write_snr_header(
    snr: &mut impl WriteBytesExt, channel_count: u8, sampling_rate: u16,
    total_samples : u32, size: u32) -> Result<(), std::io::Error>
{
    // EA_XAS enum value https://wiki.multimedia.cx/index.php?title=EA_Command_And_Conquer_3_Audio_Codec
    snr.write_u8(4)?;
    let c = (channel_count - 1) * 4;
    snr.write_u8(c)?;
    snr.write_u16::<BE>(sampling_rate)?;
    snr.write_u32::<BE>(total_samples)?; // total_samples
    snr.write_u32::<BE>(size)?; // total file size without first 8 bytes
    snr.write_u32::<BE>(total_samples)?; // total_samples
    Ok(())
}

/// write chunk to snr file
pub(crate) fn write_snr_chunk(snr: &mut impl WriteBytesExt, chunks: &[i16], channels: usize) -> Result<(), std::io::Error> {
    for c in 0..channels {
        let mut snr_chunk = SnrChunk::default();
        for n in 0..4 {
            let mut bci = 0;
            let mut bsi = 0;
            let mut diff = i32::MAX;
            // TODO full range is 0..=0xf but that sounds super noisy in BF
            // maybe BF have different values in second half of the table
            for ci in 0..4 {
                if ci == 4 || ci == 8 {
                    // based on the table these cases are exactly same
                    continue
                }
                for si in 0..=0xf {
                    block(chunks, channels, n, c, ci, si, &mut snr_chunk);

                    let m = 0;
                    let mut o = [i16::MIN;WAV_CHUNK_SIZE / 4];
                    snr_chunk.to_channel_data(n, &mut o);

                    let mut d = 0;
                    for i in 0..WAV_CHUNK_SIZE / 4 {
                        d += (value(chunks, channels, i, m, n, c) as i32 - o[i] as i32).abs();
                    }

                    if d < diff {
                        bci = ci;
                        bsi = si;
                        diff = d;
                    }
                }
            }

            block(chunks, channels, n, c, bci, bsi, &mut snr_chunk);
        }

        snr_chunk.write(snr)?;
    }

    Ok(())
}

/// get value from wav `chunks`
fn value(chunks: &[i16], channels: usize, i: usize, m: usize, n: usize, c: usize) -> i16 {
    chunks[(m*2+i+n*32)*channels+c]
}
/// update block in `snr_chunk` based on data in `chunks`
fn block(chunks: &[i16], channels: usize, n: usize, c: usize,
         ci : i8, si : i8, snr_chunk: &mut SnrChunk)
{
    let mut coeff = [[0,0,0,0], [0,0,0,0]];
    let mut shift = [0,0,0,0];
    let m = 0;
    let val = value(chunks, channels, 0, m, n, c);
    let coeff_index = ci;
    for i in 0..2 {
        coeff[i][n] = EA_ADPCM_TABLE[coeff_index as usize + 4 * i];
    }
    snr_chunk.header[n].offset = I_12_4::from_parts(val, coeff_index);
    let shift_index = si;
    let val = value(chunks, channels, 1, m, n, c);
    shift[n] = 20 - shift_index;
    snr_chunk.header[n].shift = I_12_4::from_parts(val, shift_index);

    for m in 0..15 {
        let s0 = value(chunks, channels, 0, m, n, c);
        let s1 = value(chunks, channels, 1, m, n, c);
        let s2 = value(chunks, channels, 2, m, n, c) as i32;
        let s3 = value(chunks, channels, 3, m, n, c) as i32;
        let val = s2 << 8;
        // TODO some rounding correction?
        // seems like the inverse is adding 0.5 to round up?
        // (0.5 because of shift by 8 to left 0x80 is 1 bit below that)
        //let val = val - 0x80;
        let pred = s1 as i32 * coeff[0][n] + s0 as i32 * coeff[1][n];
        let level = val - pred;
        let top_nibble = level >> shift[n];
        let top_nibble = (top_nibble & 0x0f) as u8;

        let val = s3 << 8;
        //let val = val - 0x80;
        let pred = s1 as i32 * coeff[0][n] + s0 as i32 * coeff[1][n];
        let level = val - pred;
        let bottom_nibble = level >> shift[n];
        let bottom_nibble = (bottom_nibble & 0x0f) as u8;

        snr_chunk.block[n][m] = ((top_nibble << 4) | bottom_nibble) as u8;
    }
}

// for some reason compiler complain about dead code, even thou it is used in tests
#[allow(dead_code, unused_imports)]
mod tests {
    use super::{SnrChunk, SnrChunkBlockHeader, block, I_12_4};

    const WAV_DATA : [i16;32] = [0,0,0,-1,-1,0,0,0,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,-1,0,0,0,0];

    /// inverse test to make sure we are doing as good as inverse of ffmpeg
    #[test]
    fn block_test(){
        let expected: SnrChunk = SnrChunk{
            header: [SnrChunkBlockHeader{
                offset: I_12_4::from_parts(0, 0),
                shift: I_12_4::from_parts(0, 12)
            },SnrChunkBlockHeader::default(),SnrChunkBlockHeader::default(),SnrChunkBlockHeader::default()],
            block: [[0x0f, 0xf0, 0, 0, 0, 0xf0, 0, 0xf0, 0, 0xf0, 0x0f, 0, 0xff, 0, 0], [0;15], [0;15], [0;15]]
        };

        let mut snr_chunk = SnrChunk::default();
        block(&WAV_DATA, 1, 0, 0, 0, 12, &mut snr_chunk);
        assert_eq!(expected.header[0].offset, snr_chunk.header[0].offset);
        assert_eq!(expected.header[0].shift, snr_chunk.header[0].shift);
        println!("{:x?}", snr_chunk.block[0]);
        assert_eq!(expected.block[0], snr_chunk.block[0]);
    }
}
