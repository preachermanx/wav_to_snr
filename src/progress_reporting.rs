
/// simple trait to report up to one progress at a time
pub trait ProgressReporting {
    fn new_report(&mut self);
    fn report(&mut self, percent: f32);
    fn finish(&mut self);
}

pub struct SimpleConsoleProgressBar{ len: usize, reporting: bool}

impl SimpleConsoleProgressBar {
    /// writes empty progress bar
    pub fn new(len: usize) -> Self {
        Self { len, reporting: false }
    }

    /// just a fancy way to show progress
    fn progress_bar_update(&self, percent: f32, clear: bool) {
        use std::io::Write;
        let len_f32 = self.len as f32;
        if clear {
            for _ in 0..self.len {
                print!("\x08");
            }
        }
        let mut i = 0.0;
        while i < len_f32 {
            let c = i / len_f32;
            if percent > c {
                print!("█");
            } else {
                print!("░");
            }
            i += 1.0;
        }
        std::io::stdout().flush().unwrap();
    }
}

impl ProgressReporting for SimpleConsoleProgressBar {
    fn new_report(&mut self) {
        if self.reporting {
            self.finish()
        }
        self.progress_bar_update(0.0, false);
        self.reporting = true;
    }

    fn report(&mut self, percent: f32) {
        if !self.reporting {
            self.new_report()
        }
        self.progress_bar_update(percent, true);
    }

    fn finish(&mut self) {
        self.progress_bar_update(1.0, self.reporting);
        println!();
        self.reporting = false;
    }
}

pub struct NoProgressReporter{}
impl ProgressReporting for NoProgressReporter {
    fn new_report(&mut self) { }
    fn report(&mut self, _percent: f32) { }
    fn finish(&mut self) { }
}
