use wav_to_snr::SimpleConsoleProgressBar;

fn main() -> Result<(), std::io::Error> {
    let path = std::env::args().skip(1).next()
        .expect("require 1 argument that is path to an wav file");
    if path == "-d" {
        let string_path = std::env::args().skip(2).next()
            .expect("require 2nd argument that is path to an snr file");
        return wav_to_snr::snr_to_wav(&string_path);
    }
    wav_to_snr::wav_to_snr(&path, SimpleConsoleProgressBar::new(80))
}
